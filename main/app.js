function distance(first, second){
    if ((Array.isArray(first) && Array.isArray(second)) != true) {
        throw new Error('InvalidType');
        
    }
    if (first.length == 0 && second.length == 0) {
        return 0;
    }
    first = Array.from(new Set(first));
    second = Array.from(new Set(second));
    let min = 0;
    let max = 0;
    let sum = 0;
    if (first.length < second.length) {
        min = first.length;
        max = second.length;
    } else {
        min = second.length;
        max = first.length;
    }
    for (let i = 0; i < first.length; i++) {
        for (let j = 0; j < second.length; j++) {
            if (first[i] === second[j]) {
                sum += 1;
            }
        }
    }
    let distanta = max - sum + min - sum;
    return distanta;
}


module.exports.distance = distance